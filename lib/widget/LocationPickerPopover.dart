import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import './EventListScreen.dart';
import './HeaderWithBar.dart';

class LocationPickerPopover extends StatelessWidget {
  LocationPickerPopover({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Container(
            width: 375.0,
            height: 812.0,
            decoration: BoxDecoration(
              color: const Color(0xd8000000),
              border: Border.all(width: 1.0, color: const Color(0xd8707070)),
            ),
          ),
          Transform.translate(
            offset: Offset(23.5, 177.5),
            child: SvgPicture.string(
              _svg_hip8mi,
              allowDrawingOutsideViewBox: true,
            ),
          ),
          Transform.translate(
            offset: Offset(310.0, 15.0),
            child:
                // Adobe XD layer: 'BackButton' (component)
                BackButton(),
          ),
          Transform.translate(
            offset: Offset(24.0, 121.0),
            child:
                // Adobe XD layer: 'Header With Bar' (component)
                HeaderWithBar(),
          ),
        ],
      ),
    );
  }
}

const String _svg_hip8mi =
    '<svg viewBox="23.5 177.5 329.0 1.0" ><path transform="translate(23.5, 177.5)" d="M 0 0 L 329 0" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
