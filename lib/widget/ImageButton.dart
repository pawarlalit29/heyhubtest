import 'package:flutter/material.dart';
import 'package:heyhub_test/values/values.dart';

class ImageButton extends StatelessWidget {
  final imageWidget;
  final VoidCallback btnOnClick;

  ImageButton({@required this.imageWidget, this.btnOnClick});

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 4.0,
      shape: CircleBorder(),
      clipBehavior: Clip.hardEdge,
      color: Colors.transparent,
      child: Ink.image(
        image: imageWidget,
        fit: BoxFit.cover,
        width: 50.0,
        height: 50.0,
        child: InkWell(
          onTap: () {
            btnOnClick();
          },
        ),
      ),
    );
  }
}
