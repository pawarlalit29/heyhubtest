import 'package:flutter/material.dart';

class EventPriceTag extends StatelessWidget {
  EventPriceTag({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Transform.translate(
          offset: Offset(-311.0, -134.0),
          child: Stack(
            children: <Widget>[
              Transform.translate(
                offset: Offset(311.0, 134.0),
                child: Container(
                  width: 49.0,
                  height: 28.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(9.0),
                    color: const Color(0xfffd6d90),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(319.0, 140.0),
                child: SizedBox(
                  width: 34.0,
                  child: Text(
                    'Free',
                    style: TextStyle(
                      fontFamily: 'Helvetica Neue',
                      fontSize: 13,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
