import 'package:flutter/material.dart';

class EventDescriptionPreview extends StatelessWidget {
  EventDescriptionPreview({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SizedBox(
          width: 327.0,
          height: 45.0,
          child: Text(
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
            style: TextStyle(
              fontFamily: 'Helvetica Neue',
              fontSize: 16,
              color: const Color(0xffffffff),
              fontWeight: FontWeight.w300,
            ),
            textAlign: TextAlign.left,
          ),
        ),
        Transform.translate(
          offset: Offset(0.0, 45.0),
          child: Text(
            'Read More',
            style: TextStyle(
              fontFamily: 'Helvetica Neue',
              fontSize: 12,
              color: const Color(0xfffd6d90),
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    );
  }
}
