import 'package:flutter/material.dart';
import 'package:heyhub_test/widget/EventCategories.dart';
import './HeaderWithBar.dart';
import 'FeaturedEventCard.dart';

class TodayScreen extends StatelessWidget {
  TodayScreen({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff201f2b),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(0.0, -42.0),
            child: Container(
              height: 333.0,
              decoration: BoxDecoration(
                color: const Color(0xff3e3d4d),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(319.0, -27.0),
            child: Container(
              width: 56.0,
              height: 187.0,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment(-1.0, 0.13),
                  end: Alignment(1.0, 0.14),
                  colors: [const Color(0x003e3d4d), const Color(0xff3e3d4d)],
                  stops: [0.0, 1.0],
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(24.0, 60.0),
            child:
                // Adobe XD layer: 'Header With Bar' (component)
                HeaderWithBar(
              title: 'Featured Event',
            ),
          ),
          Transform.translate(
            offset: Offset(24.0, 130.0),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 5,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: FeaturedEventCard(),
                );
              },
            ),
          ),
          Transform.translate(
            offset: Offset(24.0, 320.0),
            child: HeaderWithBar(title: 'Event Categories'),
          ),
          Transform.translate(
            offset: Offset(24.0, 350.0),
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: 5,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: EventCategories(),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
