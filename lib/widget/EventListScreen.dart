import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import './InterestsBar.dart';
import './LocationPicker.dart';
import './DatePickerSlider.dart';
import './FilterButton.dart';

class EventListScreen extends StatelessWidget {
  EventListScreen({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(0.0, -43.0),
            child: SvgPicture.string(
              _svg_3mj11f,
              allowDrawingOutsideViewBox: true,
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 65.0),
            child:
                // Adobe XD layer: 'Interests Bar' (component)
                InterestsBar(),
          ),
          Transform.translate(
            offset: Offset(0.0, 166.0),
            child: Container(
              width: 375.0,
              height: 646.0,
              decoration: BoxDecoration(
                color: const Color(0xff201f2b),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(86.0, 21.0),
            child:
                // Adobe XD layer: 'Location Picker' (component)
                LocationPicker(),
          ),
          Transform.translate(
            offset: Offset(0.0, 109.0),
            child:
                // Adobe XD layer: 'DatePickerSlider' (component)
                DatePickerSlider(),
          ),
          Transform.translate(
            offset: Offset(14.0, 12.0),
            child:
                // Adobe XD layer: 'BackButton' (component)
                BackButton(),
          ),
          Transform.translate(
            offset: Offset(335.0, 21.0),
            child:
                // Adobe XD layer: 'FilterButton' (component)
                FilterButton(),
          ),
        ],
      ),
    );
  }
}

const String _svg_3mj11f =
    '<svg viewBox="0.0 -43.0 375.0 152.0" ><defs><filter id="shadow"><feDropShadow dx="0" dy="3" stdDeviation="6"/></filter></defs><path transform="translate(0.0, -43.0)" d="M 0 0 L 375 0 L 375 47.69796752929688 L 375 152 L 0 152 L 0 0 Z" fill="#3e3d4d" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" filter="url(#shadow)"/></svg>';
