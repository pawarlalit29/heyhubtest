import 'package:flutter/material.dart';

class PeopleListOverview extends StatelessWidget {
  PeopleListOverview({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Expanded(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
              child: Text(
                'Alice Jacob, John Baptist and…',
                style: TextStyle(
                  fontFamily: 'Greycliff CF',
                  fontSize: 9,
                  color: const Color(0xbdffffff),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          Container(
            child: Text(
              '9 others',
              style: TextStyle(
                fontFamily: 'Greycliff CF',
                fontSize: 9,
                color: const Color(0xffffffff),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }
}
