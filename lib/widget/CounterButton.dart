import 'package:flutter/material.dart';
import 'package:heyhub_test/values/values.dart';

class CounterButton extends StatelessWidget {
  final IconData icons;
  final Color backgroundColor;
  final VoidCallback btnOnClick;

  CounterButton(
      {this.icons,
      this.backgroundColor = AppColors.primaryElement,
      this.btnOnClick});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 45,
        height: 45,
        child: ClipOval(
          child: Material(
            // button color
            child: InkWell(
              splashColor: Colors.black.withOpacity(.7), // inkwell color
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: Colors.black,
                    width: 0.7
                  )
                ),
                child: Icon(
                  icons
                  )),
              onTap: () {
                btnOnClick();
              },
            ),
          ),
        ));
  }
}
