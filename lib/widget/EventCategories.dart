import 'package:flutter/material.dart';
import 'package:heyhub_test/widget/EventSubtitle.dart';
import 'package:heyhub_test/widget/EventTitle.dart';
import 'PeopleListOverview.dart';

class EventCategories extends StatelessWidget {
  EventCategories({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        // Adobe XD layer: '5c677cf9b1dcdc0eb5f…' (shape)
        Container(
          width: MediaQuery.of(context).size.width - 48.0,
          height: 168.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            image: DecorationImage(
              image: const AssetImage('assets/images/e2ff6960e9a55ca23609f0e9ff56b1d2.png'),
              fit: BoxFit.cover,
            ),
            boxShadow: [
              BoxShadow(
                color: const Color(0x29000000),
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
        ),
        // Adobe XD layer: 'ed0284baed1955a631d…' (shape)
        Container(
          width: MediaQuery.of(context).size.width - 48.0,
          height: 168.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: const Color(0x77000000),
            boxShadow: [
              BoxShadow(
                color: const Color(0x13000000),
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
        ),
        Transform.translate(
          offset: Offset(17.0, 95.0),
          child: EventTitle(
            title: 'Day Events',
          )
        ),
        Transform.translate(
          offset: Offset(18.0, 125.0),
          child: EventSubtitle(
            title: 'Events for the community',
            fontSize: 14.0,)
        ),
      ],
    );
  }
}
