import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HeaderWithBar extends StatelessWidget {
  final title;
  final fontSize;
  HeaderWithBar({
    @required this.title,
    this.fontSize = 22.0
  }); 

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            fontFamily: 'Helvetica Neue',
            fontSize: fontSize,
            color: const Color(0xffffffff),
            fontWeight: FontWeight.w700,
          ),
          textAlign: TextAlign.left,
        ),
        Transform.translate(
          offset: Offset(1.5, 38.5),
          child: SvgPicture.string(
            _svg_86xk21,
            allowDrawingOutsideViewBox: true,
          ),
        ),
      ],
    );
  }
}

const String _svg_86xk21 =
    '<svg viewBox="1.5 38.5 9.0 1.0" ><path transform="translate(1.5, 38.5)" d="M 0 0 L 9 0" fill="none" stroke="#fd6d90" stroke-width="4" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
