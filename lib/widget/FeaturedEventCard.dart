import 'package:flutter/material.dart';
import 'package:heyhub_test/widget/EventPriceTag.dart';
import 'package:heyhub_test/widget/EventSubtitle.dart';
import 'package:heyhub_test/widget/EventTitle.dart';

import 'image_stack.dart';

class FeaturedEventCard extends StatelessWidget {
  FeaturedEventCard({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    List<String> images = ["https://upload.wikimedia.org/wikipedia/commons/a/a9/Tom_Hanks_TIFF_2019.jpg",
     'https://in.bmscdn.com/iedb/artist/images/website/poster/large/irrfan-khan-861-13-09-2017-03-40-54.jpg?1',
      'https://images.assettype.com/freepressjournal%2Fimport%2F2017%2F10%2FTom-Cruise.jpg?auto=format%2Ccompress&w=400&dpr=2.6',
       'https://savedelete.com/wp-content/uploads/2020/03/Hot-Hollywood-Actors.jpg'];

    return Stack(
      children: <Widget>[
        // Adobe XD layer: 'Rectangle 1' (shape)
        Container(
           width: MediaQuery.of(context).size.width * 0.42,
          height: 150.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12.0),
            image: DecorationImage(
              image: const AssetImage(
                  'assets/images/featuredeventcard.png'),
              fit: BoxFit.cover,
            ),
            boxShadow: [
              BoxShadow(
                color: const Color(0x14455b63),
                offset: Offset(0, 4),
                blurRadius: 16,
              ),
            ],
          ),
        ),
        // Container(
        //   width: MediaQuery.of(context).size.width * 0.42,
        //   height: 150.0,
        //   decoration: BoxDecoration(
        //     borderRadius: BorderRadius.circular(12.0),
        //     color: const Color(0x77000000),
        //     boxShadow: [
        //       BoxShadow(
        //         color: const Color(0x13000000),
        //         offset: Offset(0, 3),
        //         blurRadius: 6,
        //       ),
        //     ],
        //   ),
        // ),
        Positioned(
          bottom: 30,
          left: 15,
          child: Container(
            margin: EdgeInsets.only(top: 27),
            child: EventTitle(
              title: 'Park Cinema',
              fontSize: 19.0,
            ),
          ),
        ),
        Positioned(
          bottom: 15,
          left: 15,
          child: Container(
            margin: EdgeInsets.only(top: 27),
            child: EventSubtitle(
              title: 'Titanic - Showing',
              fontSize: 12.0,
            ),
          ),
        ),
        Positioned(
          top: 5,
          right: 10,
          child: EventPriceTag())
      ],
    );
  }
}
