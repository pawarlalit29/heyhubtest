import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class JoinChatButton extends StatelessWidget {
  JoinChatButton({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SvgPicture.string(
          _svg_16zip6,
          allowDrawingOutsideViewBox: true,
        ),
        Transform.translate(
          offset: Offset(128.0, 11.0),
          child: Text(
            'Join Chat',
            style: TextStyle(
              fontFamily: 'Helvetica Neue',
              fontSize: 15,
              color: const Color(0xffffffff),
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    );
  }
}

const String _svg_16zip6 =
    '<svg viewBox="0.0 0.0 327.0 40.0" ><defs><filter id="shadow"><feDropShadow dx="0" dy="3" stdDeviation="6"/></filter></defs><path  d="M 18.10749053955078 0 L 308.8924865722656 0 C 318.8930053710938 0 326.9999694824219 7.61115837097168 326.9999694824219 17 L 326.9999694824219 23 C 326.9999694824219 32.38883972167969 318.8930053710938 40 308.8924865722656 40 L 18.10749053955078 40 C 8.106999397277832 40 0 32.38883972167969 0 23 L 0 17 C 0 7.61115837097168 8.106999397277832 0 18.10749053955078 0 Z" fill="#2f96fc" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" filter="url(#shadow)"/></svg>';
