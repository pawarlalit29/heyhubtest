import 'package:flutter/material.dart';
import './PeopleListOverview.dart';
import 'EventPriceTag.dart';
import 'image_stack.dart';

class EventCard extends StatelessWidget {
  EventCard({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    List<String> images = [
      "https://upload.wikimedia.org/wikipedia/commons/a/a9/Tom_Hanks_TIFF_2019.jpg",
      'https://in.bmscdn.com/iedb/artist/images/website/poster/large/irrfan-khan-861-13-09-2017-03-40-54.jpg?1',
      'https://images.assettype.com/freepressjournal%2Fimport%2F2017%2F10%2FTom-Cruise.jpg?auto=format%2Ccompress&w=400&dpr=2.6',
      'https://savedelete.com/wp-content/uploads/2020/03/Hot-Hollywood-Actors.jpg'
    ];

    return Stack(
      children: <Widget>[
        // Adobe XD layer: '5c677cf9b1dcdc0eb5f…' (shape)
        Container(
          width: MediaQuery.of(context).size.width,
          height: 168.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            image: DecorationImage(
              image: const AssetImage(
                  'assets/images/5c677cf9b1dcdc0eb5f24c6667c38af4-2.png'),
              fit: BoxFit.cover,
            ),
            boxShadow: [
              BoxShadow(
                color: const Color(0x29000000),
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
        ),
        // Adobe XD layer: 'ed0284baed1955a631d…' (shape)
        Container(
          width: MediaQuery.of(context).size.width,
          height: 168.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: const Color(0x77000000),
            boxShadow: [
              BoxShadow(
                color: const Color(0x13000000),
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
        ),
        Transform.translate(
          offset: Offset(17.0, 75.0),
          child: Text(
            'Park Cinema',
            style: TextStyle(
              fontFamily: 'Helvetica Neue',
              fontSize: 25,
              color: const Color(0xffffffff),
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.left,
          ),
        ),
        Transform.translate(
          offset: Offset(17.0, 104.0),
          child: Text(
            'Titanic - Showing',
            style: TextStyle(
              fontFamily: 'Helvetica Neue',
              fontSize: 15,
              color: const Color(0x9affffff),
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.left,
          ),
        ),
        Positioned(top: 10, right: 10, child: EventPriceTag()),
        Transform.translate(
            offset: Offset(16.0, 130.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                SizedBox(
                  height: 30,
                  child: ImageStack(
                    backgroundColor: const Color(0xfffd6d90),
                    imageList: images,
                    imageRadius: 25, // Radius of each images
                    imageCount:
                        3, // Maximum number of images to be shown in stack
                    imageBorderWidth: 0, // Border width around the images
                    totalCount: images.length,
                    extraCountTextStyle:
                        TextStyle(color: Colors.white, fontSize: 10.0),
                  ),
                ),
                PeopleListOverview()
              ],
            ))
      ],
    );
  }
}
