import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RegisterButton extends StatelessWidget {
  RegisterButton({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SvgPicture.string(
          _svg_efofa9,
          allowDrawingOutsideViewBox: true,
        ),
        Transform.translate(
          offset: Offset(-8.0, -736.85),
          child: Stack(
            children: <Widget>[
              Transform.translate(
                offset: Offset(32.0, 754.0),
                child: Stack(
                  children: <Widget>[
                    SvgPicture.string(
                      _svg_fddhaw,
                      allowDrawingOutsideViewBox: true,
                    ),
                  ],
                ),
              ),
              Transform.translate(
                offset: Offset(118.0, 764.0),
                child: Text(
                  'Register on Eventbrite',
                  style: TextStyle(
                    fontFamily: 'Helvetica Neue',
                    fontSize: 15,
                    color: const Color(0xffffffff),
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

const String _svg_fddhaw =
    '<svg viewBox="0.0 0.0 327.0 40.0" ><defs><filter id="shadow"><feDropShadow dx="0" dy="3" stdDeviation="6"/></filter></defs><path  d="M 18.10749244689941 0 L 308.8925170898438 0 C 318.8930053710938 0 327 7.61115837097168 327 17 L 327 23 C 327 32.38883972167969 318.8930053710938 40 308.8925170898438 40 L 18.10749244689941 40 C 8.106999397277832 40 0 32.38883972167969 0 23 L 0 17 C 0 7.61115837097168 8.106999397277832 0 18.10749244689941 0 Z" fill="#fd6d90" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" filter="url(#shadow)"/></svg>';
const String _svg_efofa9 =
    '<svg viewBox="0.0 0.0 375.0 75.2" ><path  d="M 0 0 L 375 0 L 375 75.152099609375 L 0 75.152099609375 L 0 0 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
