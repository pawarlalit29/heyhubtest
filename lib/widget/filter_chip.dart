import 'package:flutter/material.dart';
import 'package:heyhub_test/values/values.dart';

class FilterChipWidget extends StatefulWidget {
  final title;

  FilterChipWidget({@required this.title});

  @override
  _FilterChipWidgetState createState() => _FilterChipWidgetState();
}

class _FilterChipWidgetState extends State<FilterChipWidget> {
  var _isSelected = false;
  @override
  Widget build(BuildContext context) {
    return FilterChip(
      label: Text(
        widget.title,
      ),
      labelStyle: TextStyle(
        color: Colors.white,
        fontFamily: "Helvetica Neue",
        fontWeight: FontWeight.bold,
        fontSize: 15,
      ),
      padding: EdgeInsets.only(bottom: 3, left: 5, right: 5),
      onSelected: (isSelected){
        setState(() {
          _isSelected = isSelected;
        });
      },
      showCheckmark: false,
      selected: _isSelected,
      backgroundColor: AppColors.accentElement,
      selectedColor: AppColors.primaryElement,
      disabledColor: AppColors.accentElement,
    );
  }
}
