import 'package:flutter/material.dart';
import 'package:heyhub_test/values/values.dart';

class SmallRoundedButton extends StatelessWidget {
  final btnTitle;
  final VoidCallback btnOnClick;

  SmallRoundedButton({@required this.btnTitle, this.btnOnClick});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton(
        onPressed: () {
          btnOnClick();
        },
        textColor: Colors.white,
        color: AppColors.primaryElement,
        disabledColor: Colors.grey,
        disabledTextColor: Colors.white,
        highlightColor: AppColors.secondaryElement,
        elevation: 4.0,
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(20.0)),
        child: Text(btnTitle),
      ),
    );
  }
}
