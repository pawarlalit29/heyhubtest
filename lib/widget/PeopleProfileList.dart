import 'package:flutter/material.dart';

class PeopleProfileList extends StatelessWidget {
  PeopleProfileList({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Transform.translate(
          offset: Offset(0.0, 0.41),
          child: Stack(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Transform.translate(
                    offset: Offset(0.0, -0.41),
                    child:
                        // Adobe XD layer: 'e3524537d0c034f2aab…' (shape)
                        Container(
                      width: 22.0,
                      height: 22.0,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(11.0, 11.0)),
                        image: DecorationImage(
                          image: const AssetImage(''),
                          fit: BoxFit.cover,
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x29000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(14.0, -0.41),
                    child:
                        // Adobe XD layer: '258df7b032a2dca605b…' (shape)
                        Container(
                      width: 22.0,
                      height: 22.0,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(11.0, 11.0)),
                        image: DecorationImage(
                          image: const AssetImage(''),
                          fit: BoxFit.cover,
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x29000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(29.0, -0.41),
                    child:
                        // Adobe XD layer: '1773c9e9de8506058a4…' (shape)
                        Container(
                      width: 20.0,
                      height: 22.0,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(10.0, 11.0)),
                        image: DecorationImage(
                          image: const AssetImage(''),
                          fit: BoxFit.cover,
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x29000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(41.0, -0.41),
                    child:
                        // Adobe XD layer: '1dc3d7a6a3f4aa12572…' (shape)
                        Container(
                      width: 21.0,
                      height: 22.0,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(10.5, 11.0)),
                        image: DecorationImage(
                          image: const AssetImage(''),
                          fit: BoxFit.cover,
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x29000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(55.0, -0.41),
                    child:
                        // Adobe XD layer: '1dc3d7a6a3f4aa12572…' (shape)
                        Container(
                      width: 22.0,
                      height: 22.0,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(11.0, 11.0)),
                        color: const Color(0xfffd6d90),
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0x29000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Transform.translate(
          offset: Offset(60.0, 5.0),
          child: Text(
            '+7',
            style: TextStyle(
              fontFamily: 'Helvetica Neue',
              fontSize: 9,
              color: const Color(0xffffffff),
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    );
  }
}
