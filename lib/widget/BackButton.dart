import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BackButton extends StatelessWidget {
  BackButton({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SvgPicture.string(
          _svg_ipelhd,
          allowDrawingOutsideViewBox: true,
        ),
        Transform.translate(
          offset: Offset(14.6, 15.0),
          child:
              // Adobe XD layer: 'icon_back' (shape)
              Container(
            width: 21.0,
            height: 21.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: const AssetImage(''),
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

const String _svg_ipelhd =
    '<svg viewBox="0.0 0.0 50.0 50.0" ><defs><filter id="shadow"><feDropShadow dx="0" dy="3" stdDeviation="6"/></filter></defs><path  d="M 25 0 C 38.80711364746094 0 50 11.19288158416748 50 25 C 50 38.80711364746094 38.80711364746094 50 25 50 C 11.19288158416748 50 0 38.80711364746094 0 25 C 0 11.19288158416748 11.19288158416748 0 25 0 Z" fill="#fd6c90" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" filter="url(#shadow)"/></svg>';
