import 'package:flutter/material.dart';

class FlatButton_ extends StatelessWidget {
  final title;
  final fontSize;
  final textColor;
  final VoidCallback onClick;

  FlatButton_({
    @required this.title,
    this.fontSize = 22.0,
    this.textColor = Colors.white,
    this.onClick}); 

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlatButton(
                onPressed: () => onClick(),
                color: Colors.transparent,
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0)),
                child: Text(
                  title,
                  style: TextStyle(
                    color: textColor,
                    fontSize: fontSize),
                ),
              ),
    );
  }
}