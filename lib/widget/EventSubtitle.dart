import 'package:flutter/material.dart';

class EventSubtitle extends StatelessWidget {
  final title;
  final fontSize;
  EventSubtitle({
    @required this.title,
    this.fontSize
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            fontFamily: 'Helvetica Neue',
            fontSize: fontSize != null ? fontSize : 16.0,
            color: const Color(0x80ffffff),
            fontWeight: FontWeight.w700,
          ),
          textAlign: TextAlign.left,
        ),
      ],
    );
  }
}
