import 'package:flutter/material.dart';
import 'package:heyhub_test/values/values.dart';

class RoundedIconButton extends StatelessWidget {
  final btnTitle;
  final Color backgroundColor;
  final IconData icons;
  final VoidCallback btnOnClick;

  RoundedIconButton(
      {@required this.btnTitle,
      this.icons,
      this.backgroundColor = AppColors.primaryElement,
      this.btnOnClick});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 45,
      child: RaisedButton.icon(
        onPressed: () {
          btnOnClick();
        },
        textColor: Colors.white,
        color: backgroundColor,
        disabledColor: Colors.grey,
        disabledTextColor: Colors.white,
        highlightColor: backgroundColor.withOpacity(0.5),
        elevation: 4.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(40.0))),
        icon: Icon(
          icons,
          color: Colors.white,
          size: 17,
        ),
        label: Text(
          btnTitle,
          style: TextStyle(color: Colors.white, fontSize: 17),
        ),
      ),
    );
  }
}
