import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LocationPicker extends StatelessWidget {
  LocationPicker({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: 242.0,
          height: 32.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            color: const Color(0xff1f1f2b),
            boxShadow: [
              BoxShadow(
                color: const Color(0x29000000),
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
        ),
        Transform.translate(
          offset: Offset(20.0, 6.0),
          child: Text(
            'Events in',
            style: TextStyle(
              fontFamily: 'Helvetica Neue',
              fontSize: 15,
              color: const Color(0xffffffff),
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.left,
          ),
        ),
        Transform.translate(
          offset: Offset(94.0, 6.0),
          child: Text(
            'Guinness Close',
            style: TextStyle(
              fontFamily: 'Helvetica Neue',
              fontSize: 15,
              color: const Color(0xfffd6d90),
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.left,
          ),
        ),
        Transform(
          transform: Matrix4(0.0, -1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
              1.0, 0.0, 216.63, 21.23, 0.0, 1.0),
          child:
              // Adobe XD layer: 'back (1)' (group)
              Stack(
            children: <Widget>[
              Transform.translate(
                offset: Offset(1.0, 0.0),
                child: Stack(
                  children: <Widget>[
                    Transform.translate(
                      offset: Offset(0.0, 0.0),
                      child: SvgPicture.string(
                        _svg_6f2dkl,
                        allowDrawingOutsideViewBox: true,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

const String _svg_6f2dkl =
    '<svg viewBox="0.0 0.0 8.5 12.6" ><path transform="translate(0.0, 0.0)" d="M 2.862005472183228 6.293232440948486 L 8.28545093536377 1.586451768875122 C 8.434773445129395 1.457162618637085 8.516924858093262 1.284299731254578 8.516924858093262 1.099980711936951 C 8.516924858093262 0.9155595302581787 8.434773445129395 0.7427989840507507 8.28545093536377 0.6133051514625549 L 7.810215473175049 0.2010937035083771 C 7.661110401153564 0.07139544188976288 7.461803436279297 0 7.249413013458252 0 C 7.037004470825195 0 6.837950706481934 0.07139544188976288 6.688737392425537 0.2010937333106995 L 0.231241837143898 5.804920196533203 C 0.0814499631524086 5.934823036193848 -0.000593114469666034 6.108401775360107 3.233897132304264e-06 6.292924880981445 C -0.000593114469666034 6.478266716003418 0.08130539208650589 6.651641845703125 0.231241837143898 6.781646728515625 L 6.682719707489014 12.380051612854 C 6.83193302154541 12.50974941253662 7.031004905700684 12.58114528656006 7.243521690368652 12.58114528656006 C 7.455912113189697 12.58114528656006 7.654984474182129 12.50975036621094 7.804324150085449 12.380051612854 L 8.279433250427246 11.96783924102783 C 8.58861255645752 11.69954395294189 8.58861255645752 11.26278400421143 8.279433250427246 10.99459171295166 L 2.862005472183228 6.293232440948486 Z" fill="#f2f2f2" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
