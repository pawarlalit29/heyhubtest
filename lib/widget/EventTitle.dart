import 'package:flutter/material.dart';

class EventTitle extends StatelessWidget {
  final title;
  final fontSize;
  EventTitle({
    @required this.title,
    this.fontSize
  });
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            fontFamily: 'Helvetica Neue',
            fontSize: fontSize != null ? fontSize : 24.0,
            color: const Color(0xffffffff),
            fontWeight: FontWeight.w700,
          ),
          textAlign: TextAlign.left,
        ),
      ],
    );
  }
}
