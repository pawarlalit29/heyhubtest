import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'CardBackground.dart';
import 'EventAdmin.dart';
import 'EventDatetime.dart';
import 'EventDescriptionPreview.dart';
import 'EventPriceTag.dart';
import 'EventSubtitle.dart';
import 'EventTitle.dart';
import 'HeaderWithBar.dart';
import 'JoinChatButton.dart';
import 'ParticipantsFull.dart';
import 'Participantscounter.dart';
import 'PeopleListOverview.dart';
import 'RegisterButton.dart';

class EventDetailsScreen extends StatelessWidget {
  EventDetailsScreen({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff201f2b),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(0.0, 453.0),
            child: Container(
              width: 375.0,
              height: 436.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(40.0),
                  bottomLeft: Radius.circular(40.0),
                ),
                color: const Color(0xff1f1e2a),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 283.0),
            child:
                // Adobe XD layer: 'Card Background' (component)
                CardBackground(),
          ),
          Stack(
            children: <Widget>[
              Transform.translate(
                offset: Offset(24.0, 530.0),
                child:
                    // Adobe XD layer: 'EventDescriptionPre…' (component)
                    EventDescriptionPreview(),
              ),
            ],
          ),
          Transform.translate(
            offset: Offset(24.0, 398.0),
            child:
                // Adobe XD layer: 'Event Datetime' (component)
                EventDatetime(),
          ),
          Transform.translate(
            offset: Offset(168.0, 651.0),
            child:
                // Adobe XD layer: 'Participants counter' (component)
                Participantscounter(),
          ),
          Transform.translate(
            offset: Offset(103.0, 766.0),
            child:
                // Adobe XD layer: 'Event Admin' (component)
                EventAdmin(),
          ),
          Transform.translate(
            offset: Offset(24.0, 834.0),
            child:
                // Adobe XD layer: 'Join Chat Button' (component)
                JoinChatButton(),
          ),
          Transform.translate(
            offset: Offset(-55.0, -576.0),
            child: Stack(
              children: <Widget>[
                Transform.translate(
                  offset: Offset(320.0, 637.0),
                  child: Container(
                    width: 30.0,
                    height: 30.0,
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.all(Radius.elliptical(15.0, 15.0)),
                      color: const Color(0xff3e3d4d),
                      boxShadow: [
                        BoxShadow(
                          color: const Color(0x29000000),
                          offset: Offset(0, 3),
                          blurRadius: 6,
                        ),
                      ],
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(325.37, 643.46),
                  child:
                      // Adobe XD layer: 'share' (group)
                      Stack(
                    children: <Widget>[
                      SvgPicture.string(
                        _svg_kbtq99,
                        allowDrawingOutsideViewBox: true,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(9.0, 61.0),
            child:
                // Adobe XD layer: 'BackButton' (component)
                BackButton(),
          ),
          Stack(
            children: <Widget>[
              // Adobe XD layer: '5c677cf9b1dcdc0eb5f…' (shape)
              Container(
                width: 375.0,
                height: 337.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(40.0),
                    bottomLeft: Radius.circular(40.0),
                  ),
                  image: DecorationImage(
                    image: const AssetImage(''),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
          Transform.translate(
            offset: Offset(0.0, 111.0),
            child: Container(
              width: 375.0,
              height: 226.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(40.0),
                  bottomLeft: Radius.circular(40.0),
                ),
                gradient: LinearGradient(
                  begin: Alignment(0.0, -1.0),
                  end: Alignment(0.0, 1.0),
                  colors: [const Color(0x00000000), const Color(0xff000000)],
                  stops: [0.0, 1.0],
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(24.0, 214.0),
            child:
                // Adobe XD layer: 'EventTitle' (component)
                EventTitle(),
          ),
          Transform.translate(
            offset: Offset(24.0, 250.0),
            child:
                // Adobe XD layer: 'Event Subtitle' (component)
                EventSubtitle(),
          ),
          Transform.translate(
            offset: Offset(14.0, 13.0),
            child:
                // Adobe XD layer: 'BackButton' (component)
                BackButton(),
          ),
          Transform.translate(
            offset: Offset(24.0, 700.0),
            child:
                // Adobe XD layer: 'Participants Full' (component)
                ParticipantsFull(),
          ),
          Transform.translate(
            offset: Offset(24.0, 291.0),
            child:
                // Adobe XD layer: 'PeopleListOverview' (component)
                PeopleListOverview(),
          ),
          Transform.translate(
            offset: Offset(24.0, 474.0),
            child:
                // Adobe XD layer: 'Header With Bar' (component)
                HeaderWithBar(),
          ),
          Transform.translate(
            offset: Offset(24.0, 645.0),
            child:
                // Adobe XD layer: 'Header With Bar' (component)
                HeaderWithBar(),
          ),
          Transform.translate(
            offset: Offset(0.0, 907.85),
            child:
                // Adobe XD layer: 'Register Button' (component)
                RegisterButton(),
          ),
          Transform.translate(
            offset: Offset(312.0, 15.0),
            child:
                // Adobe XD layer: 'Event Price Tag' (component)
                EventPriceTag(),
          ),
        ],
      ),
    );
  }
}

const String _svg_kbtq99 =
    '<svg viewBox="0.0 0.0 15.7 17.2" ><path transform="translate(-304.11, -15.46)" d="M 319.3210754394531 18.32654190063477 C 319.3210754394531 19.61152267456055 318.279541015625 20.6532154083252 316.9945678710938 20.6532154083252 C 315.7095336914062 20.6532154083252 314.66796875 19.61152267456055 314.66796875 18.32654190063477 C 314.66796875 17.04169464111328 315.7095336914062 16 316.9945678710938 16 C 318.279541015625 16 319.3210754394531 17.04169464111328 319.3210754394531 18.32654190063477 Z M 319.3210754394531 18.32654190063477" fill="#fd6d90" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(-288.65, 0.0)" d="M 301.5314331054688 5.727024078369141 C 299.9521484375 5.727024078369141 298.66796875 4.442699909210205 298.66796875 2.863446235656738 C 298.66796875 1.284323453903198 299.9521484375 0 301.5314331054688 0 C 303.1106872558594 0 304.3948669433594 1.284323453903198 304.3948669433594 2.863446235656738 C 304.3948669433594 4.442699909210205 303.1106872558594 5.727024078369141 301.5314331054688 5.727024078369141 Z M 301.5314331054688 1.073808789253235 C 300.5442504882812 1.073808789253235 299.7417602539062 1.877068161964417 299.7417602539062 2.863446235656738 C 299.7417602539062 3.849955797195435 300.5442504882812 4.653214931488037 301.5314331054688 4.653214931488037 C 302.5185852050781 4.653214931488037 303.321044921875 3.849955797195435 303.321044921875 2.863446235656738 C 303.321044921875 1.877068161964417 302.5185852050781 1.073808789253235 301.5314331054688 1.073808789253235 Z M 301.5314331054688 1.073808789253235" fill="#fd6d90" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(-304.11, -345.34)" d="M 319.3210754394531 359.6587219238281 C 319.3210754394531 360.9435424804688 318.279541015625 361.9852294921875 316.9945678710938 361.9852294921875 C 315.7095336914062 361.9852294921875 314.66796875 360.9435424804688 314.66796875 359.6587219238281 C 314.66796875 358.3737487792969 315.7095336914062 357.33203125 316.9945678710938 357.33203125 C 318.279541015625 357.33203125 319.3210754394531 358.3737487792969 319.3210754394531 359.6587219238281 Z M 319.3210754394531 359.6587219238281" fill="#fd6d90" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(-288.65, -329.88)" d="M 301.5314331054688 347.05908203125 C 299.9521484375 347.05908203125 298.66796875 345.7747192382812 298.66796875 344.1956176757812 C 298.66796875 342.6163330078125 299.9521484375 341.33203125 301.5314331054688 341.33203125 C 303.1106872558594 341.33203125 304.3948669433594 342.6163330078125 304.3948669433594 344.1956176757812 C 304.3948669433594 345.7747192382812 303.1106872558594 347.05908203125 301.5314331054688 347.05908203125 Z M 301.5314331054688 342.4058837890625 C 300.5442504882812 342.4058837890625 299.7417602539062 343.2091064453125 299.7417602539062 344.1956176757812 C 299.7417602539062 345.1820068359375 300.5442504882812 345.9852905273438 301.5314331054688 345.9852905273438 C 302.5185852050781 345.9852905273438 303.321044921875 345.1820068359375 303.321044921875 344.1956176757812 C 303.321044921875 343.2091064453125 302.5185852050781 342.4058837890625 301.5314331054688 342.4058837890625 Z M 301.5314331054688 342.4058837890625" fill="#fd6d90" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(-15.46, -180.4)" d="M 20.6532154083252 188.9945068359375 C 20.6532154083252 190.2794799804688 19.61152267456055 191.321044921875 18.32654190063477 191.321044921875 C 17.04169464111328 191.321044921875 16 190.2794799804688 16 188.9945068359375 C 16 187.7095336914062 17.04169464111328 186.66796875 18.32654190063477 186.66796875 C 19.61152267456055 186.66796875 20.6532154083252 187.7095336914062 20.6532154083252 188.9945068359375 Z M 20.6532154083252 188.9945068359375" fill="#fd6d90" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(0.0, -164.94)" d="M 2.863446235656738 176.3948822021484 C 1.284323453903198 176.3948822021484 0 175.1106719970703 0 173.5314331054688 C 0 171.9521636962891 1.284323453903198 170.6679840087891 2.863446235656738 170.6679840087891 C 4.442699909210205 170.6679840087891 5.727024078369141 171.9521636962891 5.727024078369141 173.5314331054688 C 5.727024078369141 175.1106719970703 4.442699909210205 176.3948822021484 2.863446235656738 176.3948822021484 Z M 2.863446235656738 171.7417755126953 C 1.876281499862671 171.7417755126953 1.073808789253235 172.5449066162109 1.073808789253235 173.5314331054688 C 1.073808789253235 174.5179443359375 1.876281499862671 175.3210906982422 2.863446235656738 175.3210906982422 C 3.850742101669312 175.3210906982422 4.653214931488037 174.5179443359375 4.653214931488037 173.5314331054688 C 4.653214931488037 172.5449066162109 3.850742101669312 171.7417755126953 2.863446235656738 171.7417755126953 Z M 2.863446235656738 171.7417755126953" fill="#fd6d90" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(-110.51, -87.19)" d="M 115.0651016235352 95.43421936035156 C 114.8159255981445 95.43421936035156 114.5739364624023 95.30459594726562 114.4422225952148 95.07270812988281 C 114.2467803955078 94.72979736328125 114.3671112060547 94.29238891601562 114.7100067138672 94.09616088867188 L 121.3524780273438 90.30924987792969 C 121.6954040527344 90.11236572265625 122.1328048706055 90.23269653320312 122.3290405273438 90.57691955566406 C 122.5244827270508 90.91983032226562 122.4041519165039 91.35723876953125 122.0612411499023 91.553466796875 L 115.4186401367188 95.34037780761719 C 115.3069381713867 95.40408325195312 115.1853103637695 95.43421936035156 115.0651016235352 95.43421936035156 Z M 115.0651016235352 95.43421936035156" fill="#fd6d90" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(-110.53, -257.29)" d="M 121.7286529541016 271.4382934570312 C 121.6083068847656 271.4382934570312 121.4866790771484 271.4081726074219 121.375 271.3444519042969 L 114.7323760986328 267.5575561523438 C 114.3894653320312 267.3621215820312 114.2692565917969 266.9247131347656 114.4647064208984 266.5810241699219 C 114.6593627929688 266.2374572753906 115.0974426269531 266.1164855957031 115.4411163330078 266.3133544921875 L 122.083740234375 270.1002807617188 C 122.4266510009766 270.2957153320312 122.5468444824219 270.7330932617188 122.3514099121094 271.0768127441406 C 122.2190093994141 271.3086853027344 121.9770355224609 271.4382934570312 121.7286529541016 271.4382934570312 Z M 121.7286529541016 271.4382934570312" fill="#fd6d90" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
