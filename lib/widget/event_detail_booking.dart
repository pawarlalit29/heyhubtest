import 'package:flutter/material.dart';

import 'CounterButton.dart';
import 'EventDate.dart';
import 'RoundedButton.dart';
import 'RoundedIconButton.dart';

class EventBooking extends StatefulWidget {
  @override
  _EventBookingState createState() => _EventBookingState();
}

class _EventBookingState extends State<EventBooking>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animationBtn;
  int _counter = 0;
  bool isRegister = false;

  void incrementCount() {
    setState(() {
      _counter++;
    });
  }

  void decrementCount() {
    setState(() {
      if (_counter > 0) _counter--;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 700), vsync: this);
    animationBtn = CurvedAnimation(parent: controller, curve: Curves.easeIn);
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return isRegister? bookingConfirmation() : bookingWindow();
  }

  Widget bookingWindow() {
    return Container(
      height: 250,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0))),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Stack(
              children: <Widget>[
                Positioned(
                  top: 10,
                  left: 0,
                  right: 0,
                  child: Center(
                    child: Container(
                      height: 3,
                      width: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(1.0)),
                          color: Colors.grey[400].withOpacity(0.8)),
                    ),
                  ),
                ),
                Positioned(
                  top: 50,
                  left: 0,
                  right: 0,
                  child: Center(
                    child: Text(
                      'Bringing guests?',
                      style: TextStyle(
                        fontFamily: 'Helvetica Neue',
                        fontSize: 25,
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
                Positioned(
                  top: 100,
                  left: 30,
                  right: 30,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: CounterButton(
                          icons: Icons.remove,
                          btnOnClick: () {
                            decrementCount();
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Container(
                          width: 90,
                          child: Center(
                            child: Text(
                              '$_counter',
                              style: TextStyle(
                                fontSize: 45,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: CounterButton(
                          icons: Icons.add,
                          btnOnClick: () {
                            incrementCount();
                          },
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: FadeTransition(
              opacity: animationBtn,
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 22),
                  child: RoundedButton(
                    btnTitle: 'Register on Eventbrite',
                    btnOnClick: () {
                      setState(() {
                        isRegister = true;
                      });
                    },
                  )),
            ),
          )
        ],
      ),
    );
  }

  Widget bookingConfirmation() {
    return FadeTransition(
      opacity: animationBtn,
          child: Container(
          height: 320,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40.0),
                  topRight: Radius.circular(40.0))),
          child: Stack(
            children: <Widget>[
              Positioned(
                top: 10,
                left: 0,
                right: 0,
                child: Center(
                  child: Container(
                    height: 3,
                    width: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(1.0)),
                        color: Colors.grey[400].withOpacity(0.8)),
                  ),
                ),
              ),
              Positioned(
                top: 50,
                left: 0,
                right: 0,
                child: Center(
                  child: Text(
                    'You +$_counter guest are going!',
                    style: TextStyle(
                      fontFamily: 'Helvetica Neue',
                      fontSize: 25,
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              Positioned(
                top: 100,
                left: 0,
                right: 0,
                child: EventDate()),

              Positioned(
                bottom: 60,
                left: 0,
                right: 0,
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 22),
                    child: RoundedIconButton(
                      btnTitle: 'Tell Friends',
                      icons: Icons.share,
                      backgroundColor: Colors.grey[500].withOpacity(0.7),
                      btnOnClick: () {
                        //displayBottomSheet(context);
                      },
                    )),
              ),
              // Done button
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 22),
                    child: RoundedButton(
                      btnTitle: 'Done',
                      btnOnClick: () {
                        //displayBottomSheet(context);
                      },
                    )),
              )
            ],
          )),
    );
  }
}
