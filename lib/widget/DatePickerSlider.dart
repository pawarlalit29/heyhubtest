import 'package:flutter/material.dart';

class DatePickerSlider extends StatelessWidget {
  DatePickerSlider({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: 375.0,
          height: 57.0,
          decoration: BoxDecoration(
            color: const Color(0xfffd6d90),
            boxShadow: [
              BoxShadow(
                color: const Color(0x29000000),
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
        ),
        Transform.translate(
          offset: Offset(96.0, 28.0),
          child: Container(
            width: 20.0,
            height: 20.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.elliptical(10.0, 10.0)),
              color: const Color(0xffd5577d),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(0.0, -109.0),
          child: Stack(
            children: <Widget>[
              Transform.translate(
                offset: Offset(16.0, 125.0),
                child: Container(
                  width: 55.0,
                  height: 25.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(13.0),
                    color: const Color(0xffd5577d),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(32.0, 129.0),
                child: Text(
                  'Jun',
                  style: TextStyle(
                    fontFamily: 'Helvetica Neue',
                    fontSize: 13,
                    color: const Color(0xffffffff),
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ],
          ),
        ),
        Transform.translate(
          offset: Offset(292.0, 0.0),
          child: Container(
            width: 83.0,
            height: 57.0,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(1.0, 0.25),
                end: Alignment(-1.0, 0.25),
                colors: [const Color(0xfffd6d90), const Color(0x00fd6d90)],
                stops: [0.0, 1.0],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
