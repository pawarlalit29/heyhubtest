import 'package:flutter/material.dart';

class CardBackground extends StatelessWidget {
  CardBackground({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: 375.0,
          height: 339.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(40.0),
              bottomLeft: Radius.circular(40.0),
            ),
            color: const Color(0xff3e3d4d),
          ),
        ),
      ],
    );
  }
}
