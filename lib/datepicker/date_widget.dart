import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'tap.dart';

class DateWidget extends StatelessWidget {
  final DateTime date;
  final double dateSize, daySize, monthSize;
  final Color dateColor, monthColor, dayColor;
  final Color selectionColor;
  final DateSelectionCallback onDateSelected;

  DateWidget(
      {@required this.date,
      @required this.dateSize,
      @required this.daySize,
      @required this.monthSize,
      @required this.dateColor,
      @required this.monthColor,
      @required this.dayColor,
      @required this.selectionColor,
      this.onDateSelected});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Container(
            margin: EdgeInsets.all(3.0),
            child: Padding(
                padding: const EdgeInsets.only(
                    top: 8.0, bottom: 8.0, left: 12, right: 12),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                          new DateFormat("E")
                              .format(date)
                              .toUpperCase(), // Month
                          style: TextStyle(
                            color: monthColor,
                            fontSize: monthSize,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w500,
                          )),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Container(
                          width: 20,
                          height: 20,
                          decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            color: selectionColor,
                          ),
                          child: Center(
                            child: Text(date.day.toString(), // Date
                                style: TextStyle(
                                  color: dateColor,
                                  fontSize: dateSize,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.w700,
                                )),
                          ),
                        ),
                      ),
                    ]))),
        onTap: () {
          // Check if onDateSelected is not null
          if (onDateSelected != null) {
            // Call the onDateSelected Function
            onDateSelected(this.date);
          }
        });
  }
}
