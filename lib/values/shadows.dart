/*
*  shadows.dart
*  EventFlowFlutterAssets
*
*  Created by lalit.
*  Copyright © 2018 lalitpawar. All rights reserved.
    */

import 'package:flutter/rendering.dart';


class Shadows {
  static const BoxShadow primaryShadow = BoxShadow(
    color: Color.fromARGB(41, 0, 0, 0),
    offset: Offset(0, 3),
    blurRadius: 6,
  );
}