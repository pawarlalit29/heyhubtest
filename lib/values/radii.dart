/*
*  radii.dart
*  EventFlowFlutterAssets
*
*  Created by lalit.
*  Copyright © 2018 lalitpawar. All rights reserved.
    */

import 'package:flutter/rendering.dart';


class Radii {
  static const BorderRadiusGeometry k14pxRadius = BorderRadius.all(Radius.circular(14.5));
}