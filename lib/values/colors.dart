/*
*  colors.dart
*  EventFlowFlutterAssets
*
*  Created by lalit.
*  Copyright © 2018 lalitpawar. All rights reserved.
    */

import 'dart:ui';


class AppColors {
  static const Color primaryBackground = Color.fromARGB(255, 0, 0, 0);
  static const Color secondaryBackground = Color.fromARGB(255, 62, 61, 77);
  static const Color ternaryBackground = Color.fromARGB(255, 32, 31, 43);
  static const Color primaryElement = Color.fromARGB(255, 253, 109, 144);
  static const Color secondaryElement = Color.fromARGB(255, 31, 31, 43);
  static const Color accentElement = Color.fromARGB(255, 213, 87, 125);
  static const Color primaryText = Color.fromARGB(255, 255, 255, 255);
  static const Color secondaryText = Color.fromARGB(255, 253, 109, 144);
}