import 'package:flutter/material.dart';
import 'package:heyhub_test/widget/FeaturedEventCard.dart';
import 'package:heyhub_test/widget/HeaderWithBar.dart';
import 'package:heyhub_test/widget/ImageButton.dart';

class LocationScreen extends StatelessWidget {
  final VoidCallback onClose;

  LocationScreen({@required this.onClose});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 50),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Align(
              alignment: Alignment.topRight,
              child: ImageButton(
                imageWidget: AssetImage('assets/images/backbutton-2.png'),
                btnOnClick: () {
                  onClose();
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: HeaderWithBar(title: ' Which Location'),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 2,
                decoration: BoxDecoration(color: Colors.grey[700]),
              ),
            ),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.only(top: 10, left: 10.0, right: 14.0),
                scrollDirection: Axis.vertical,
                itemCount: 25,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: Padding(
                      padding: EdgeInsets.only(top:10,bottom: 10),
                      child: Text(
                        'Location ${index + 1}',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w600
                        ),
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
