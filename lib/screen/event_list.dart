import 'package:flutter/material.dart';
import 'package:heyhub_test/datepicker/date_picker_timeline.dart';
import 'package:heyhub_test/screen/event_detail.dart';
import 'package:heyhub_test/values/values.dart';
import 'package:heyhub_test/widget/EventCard.dart';
import 'package:heyhub_test/widget/HeaderWithBar.dart';
import 'package:heyhub_test/widget/ImageButton.dart';
import 'package:heyhub_test/widget/filter_chip.dart';
import 'package:sticky_headers/sticky_headers.dart';

import 'location_search.dart';

class EventList extends StatefulWidget {
  const EventList({Key key}) : super(key: key);

  @override
  _EventListState createState() => _EventListState();
}

class _EventListState extends State<EventList> {
  bool isSearchView = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(0xff201f2b),
        body: isSearchView
            ? LocationScreen(
                onClose: () {
                  setState(() {
                    isSearchView = false;
                  });
                },
              )
            : showEventList(context));
  }

  Widget showEventList(BuildContext context) {
    DateTime _selectedValue = DateTime.now();
    return Container(
      child: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Transform.translate(
                offset: Offset(0.0, 0.0),
                child: Container(
                  height: 215,
                  decoration: BoxDecoration(
                      color: AppColors
                          .secondaryBackground //const Color(0xff3e3d4d),
                      ),
                ),
              ),
              Transform.translate(
                offset: Offset(15.0, 50),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    ImageButton(
                      imageWidget: AssetImage('assets/images/backbutton.png'),
                    ),
                    Expanded(
                      child: Container(
                        height: 32,
                        margin: EdgeInsets.only(top: 0, right: 7, left: 40),
                        decoration: BoxDecoration(
                          color: AppColors.secondaryElement,
                          boxShadow: [
                            Shadows.primaryShadow,
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 20),
                              child: Text(
                                "Events in",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: AppColors.primaryText,
                                  fontFamily: "Helvetica Neue",
                                  fontWeight: FontWeight.w700,
                                  fontSize: 15,
                                ),
                              ),
                            ),
                            Spacer(),
                            Material(
                              type: MaterialType.transparency,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              color: Colors.transparent,
                              child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      isSearchView = true;
                                    });
                                  },
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                  focusColor: Colors.transparent,
                                  splashColor: Colors.black
                                      .withOpacity(.7), // inkwell color
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(right: 12),
                                        child: Text(
                                          "Guinness Close",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            color: AppColors.secondaryText,
                                            fontFamily: "Helvetica Neue",
                                            fontWeight: FontWeight.w700,
                                            fontSize: 15,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 10,
                                        height: 14,
                                        margin:
                                            EdgeInsets.only(top: 10, right: 20),
                                        child: RotatedBox(
                                          quarterTurns: 3,
                                          child: Icon(
                                            Icons.arrow_back_ios,
                                            color: Colors.white,
                                            size: 18,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: 32,
                      height: 32,
                      margin: EdgeInsets.only(top: 0, right: 25),
                      child: Image.asset(
                        "assets/images/filterbutton.png",
                        fit: BoxFit.none,
                      ),
                    ),
                  ],
                ),
              ),
              Transform.translate(
                offset: Offset(0.0, 110.0),
                child: SizedBox(
                  height: 30,
                  child: ListView.builder(
                    padding: EdgeInsets.only(left: 15.0, right: 14.0),
                    scrollDirection: Axis.horizontal,
                    itemCount: 8,
                    itemBuilder: (context, index) {
                      return Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: FilterChipWidget(
                            title: 'Dance',
                          ));
                    },
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(0, 155),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 60,
                  decoration: BoxDecoration(
                    color: AppColors.primaryElement,
                    boxShadow: [
                      Shadows.primaryShadow,
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            width: 55,
                            height: 25,
                            decoration: BoxDecoration(
                              color: AppColors.accentElement,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12.5)),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 16),
                                  child: Text(
                                    "Jun",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: AppColors.primaryText,
                                      fontFamily: "Helvetica Neue",
                                      fontWeight: FontWeight.w700,
                                      fontSize: 13,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: DatePickerTimeline(_selectedValue,
                                dateSize: 12, onDateChange: (date) {}),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            flex: 1,
            child: Container(
              child: ListView.builder(
                padding: EdgeInsets.only(left: 10.0, right: 10.0),
                scrollDirection: Axis.vertical,
                itemCount: 5,
                itemBuilder: (context, index) {
                  return StickyHeader(
                    header: Container(
                      height: 70.0,
                      color: AppColors.secondaryElement,
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
                      alignment: Alignment.centerLeft,
                      child: HeaderWithBar(
                        title: 'Web 1 July',
                      ),
                    ),
                    overlapHeaders: true,
                    content: Transform.translate(
                      offset: Offset(0, 30),
                      child: Container(
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: ClampingScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          itemCount: 2,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(10),
                              child: Material(
                                  type: MaterialType.transparency,
                                  elevation: 6.0,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                  color: Colors.transparent,
                                  child: InkWell(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  EventDetail()),
                                        );
                                      },
                                      child: EventCard())),
                            );
                          },
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
