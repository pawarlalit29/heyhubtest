import 'package:flutter/material.dart';
import 'package:heyhub_test/values/values.dart';
import 'package:heyhub_test/widget/EventCategories.dart';
import 'package:heyhub_test/widget/FeaturedEventCard.dart';
import 'package:heyhub_test/widget/HeaderWithBar.dart';

class TodayScreen_ extends StatelessWidget {
  const TodayScreen_({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
          child: Scaffold(
        backgroundColor: const Color(0xff201f2b),
        body: ListView(
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Transform.translate(
                  offset: Offset(0.0, -50.0),
                  child: Container(
                    height: MediaQuery.of(context).size.height * .36,
                    decoration: BoxDecoration(
                      color: AppColors.secondaryBackground//const Color(0xff3e3d4d),
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(24.0, 20.0),
                  child:
                      // Adobe XD layer: 'Header With Bar' (component)
                      HeaderWithBar(
                    title: 'Featured Event',
                  ),
                ),
                Transform.translate(
                  offset: Offset(0.0, 90.0),
                  child: SizedBox(
                    height: 150,
                    child: ListView.builder(
                      padding: EdgeInsets.only(left: 24.0, right: 14.0),
                      scrollDirection: Axis.horizontal,
                      itemCount: 5,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: FeaturedEventCard(),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
            Transform.translate(
              offset: Offset(0, -15),
                          child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: HeaderWithBar(title: 'Event Categories'),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 24.0, vertical: 30.0),
              child: ListView.builder(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: 5,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 20.0),
                    child: EventCategories(),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
