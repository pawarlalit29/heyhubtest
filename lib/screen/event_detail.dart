import 'package:flutter/material.dart';
import 'package:heyhub_test/values/values.dart';
import 'package:heyhub_test/widget/EventAdmin.dart';
import 'package:heyhub_test/widget/EventDatetime.dart';
import 'package:heyhub_test/widget/EventDescriptionPreview.dart';
import 'package:heyhub_test/widget/EventPriceTag.dart';
import 'package:heyhub_test/widget/HeaderWithBar.dart';
import 'package:heyhub_test/widget/ImageButton.dart';
import 'package:heyhub_test/widget/PeopleListOverview.dart';
import 'package:heyhub_test/widget/RoundedButton.dart';
import 'package:heyhub_test/widget/event_detail_booking.dart';
import 'package:heyhub_test/widget/filter_chip.dart';
import 'package:heyhub_test/widget/flat_button.dart';
import 'package:heyhub_test/widget/image_stack.dart';

class EventDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<String> images = [
      "https://upload.wikimedia.org/wikipedia/commons/a/a9/Tom_Hanks_TIFF_2019.jpg",
      'https://in.bmscdn.com/iedb/artist/images/website/poster/large/irrfan-khan-861-13-09-2017-03-40-54.jpg?1',
      'https://images.assettype.com/freepressjournal%2Fimport%2F2017%2F10%2FTom-Cruise.jpg?auto=format%2Ccompress&w=400&dpr=2.6',
      'https://savedelete.com/wp-content/uploads/2020/03/Hot-Hollywood-Actors.jpg',
    ];

    void displayBottomSheet(BuildContext context) {
      showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(top: Radius.circular(40))),
          backgroundColor: Colors.transparent,
          elevation: 10.0,
          builder: (ctx) {
            return EventBooking();
          });
    }

    return Scaffold(
        backgroundColor: AppColors.secondaryBackground,
        body: SafeArea(
            top: false,
            bottom: false,
            child: Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: ListView(
                    children: <Widget>[
                      Transform.translate(
                        offset: Offset(0, -50),
                        child: Stack(
                          children: <Widget>[
                            Container(
                              height: 400,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(40),
                                  //color: AppColors.secondaryBackground
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/group-4.png'),
                                      fit: BoxFit.cover)),
                            ),
                            Transform.translate(
                              offset: Offset(0, 250),
                              child: Container(
                                height: 150,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(40),
                                        bottomRight: Radius.circular(40)),
                                    gradient: LinearGradient(
                                        colors: [
                                          const Color(0x77000000),
                                          const Color(0x77000000)
                                              .withOpacity(0.5),
                                          const Color(0x77000000)
                                              .withOpacity(0.1)
                                        ],
                                        begin: Alignment.bottomCenter,
                                        end: Alignment.topCenter,
                                        stops: [0.2, 0.6, 1])
                                    //color: AppColors.secondaryBackground
                                    ),
                              ),
                            ),
                            Positioned(
                              bottom: 90,
                              left: 30,
                              right: 20,
                              child: Text(
                                'Park Cinema',
                                style: TextStyle(
                                  fontFamily: 'Helvetica Neue',
                                  fontSize: 25,
                                  color: const Color(0xffffffff),
                                  fontWeight: FontWeight.w700,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Positioned(
                              bottom: 70,
                              left: 30,
                              right: 20,
                              child: Text(
                                'Titanic - Showing',
                                style: TextStyle(
                                  fontFamily: 'Helvetica Neue',
                                  fontSize: 15,
                                  color: const Color(0x9affffff),
                                  fontWeight: FontWeight.w700,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Positioned(
                                top: 70, right: 20, child: EventPriceTag()),
                            Positioned(
                                bottom: 30,
                                left: 30,
                                right: 20,
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 30,
                                      child: ImageStack(
                                        backgroundColor:
                                            const Color(0xfffd6d90),
                                        imageList: images,
                                        imageRadius:
                                            25, // Radius of each images
                                        imageCount:
                                            3, // Maximum number of images to be shown in stack
                                        imageBorderWidth:
                                            0, // Border width around the images
                                        totalCount: images.length,
                                        extraCountTextStyle: TextStyle(
                                            color: Colors.white,
                                            fontSize: 10.0),
                                      ),
                                    ),
                                    PeopleListOverview()
                                  ],
                                )),
                          ],
                        ),
                      ),
                      // add filters
                      Transform.translate(
                        offset: Offset(0, -20),
                        child: SizedBox(
                          height: 30,
                          child: ListView.builder(
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            padding: EdgeInsets.only(left: 30.0, right: 14.0),
                            scrollDirection: Axis.horizontal,
                            itemCount: 8,
                            itemBuilder: (context, index) {
                              return Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: FilterChipWidget(
                                    title: 'Dance',
                                  ));
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30, vertical: 10),
                        child: EventDatetime(),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30, vertical: 10),
                        child: HeaderWithBar(title: 'Event Information'),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30, vertical: 10),
                        child: EventDescriptionPreview(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 40.0),
                        child: Stack(
                          children: <Widget>[
                            Container(
                              height: 300,
                              decoration: BoxDecoration(
                                  color: AppColors.ternaryBackground),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 10),
                              child: HeaderWithBar(title: 'Participants'),
                            ),
                            Positioned(
                              top: 15,
                              left: 180,
                              child: Text(
                                '(11/12)',
                                style: TextStyle(
                                  fontFamily: 'Helvetica Neue',
                                  fontSize: 12,
                                  color: const Color(0xffffffff),
                                  fontWeight: FontWeight.w300,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            // participant images
                            Positioned(
                                top: 65,
                                left: 30,
                                right: 30,
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(
                                      height: 50,
                                      child: ImageStack(
                                        backgroundColor:
                                            const Color(0xfffd6d90),
                                        imageList: images,
                                        imageRadius:
                                            50, // Radius of each images
                                        imageCount:
                                            3, // Maximum number of images to be shown in stack
                                        imageBorderWidth:
                                            0, // Border width around the images
                                        totalCount: images.length,
                                        extraCountTextStyle: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Expanded(
                                        child: FlatButton_(
                                      title: 'Sell all',
                                      fontSize: 16.0,
                                    )),
                                  ],
                                )),
                            Positioned(
                                top: 140,
                                left: 30,
                                right: 30,
                                child: EventAdmin()),
                            Positioned(
                              top: 220,
                              left: 30,
                              right: 30,
                              child: RoundedButton(
                                btnTitle: 'Join Chat',
                                backgroundColor: const Color(0Xff2f96fc),
                                btnOnClick: () {
                                  //displayBottomSheet(context);
                                },
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Positioned(
                  top: 50,
                  left: 20,
                  child: ImageButton(
                      imageWidget: AssetImage('assets/images/backbutton.png'),
                      btnOnClick: () {
                        print('image button');
                        Navigator.pop(context);
                      }),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    height: 85,
                    decoration:
                        BoxDecoration(color: Colors.grey[700].withOpacity(0.9)),
                    child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 30, vertical: 22),
                        child: RoundedButton(
                          btnTitle: 'Register on Eventbrite',
                          btnOnClick: () {
                            displayBottomSheet(context);
                          },
                        )),
                  ),
                ),
              ],
            )));
  }
}
